package main

import (
	"fmt"

	"gitlab.com/gomod/b"
	"gitlab.com/gomod/c"
)

func main() {
	Say(0)
}

func Say(l int) {
	fmt.Printf("%v%v\n", "                       "[:l*2], "A: 1")
	b.Say(l + 1)
	c.Say(l + 1)
}
